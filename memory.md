# Notable memory addresses

```
0x00000000 - Beginning of code ROM, Vector table STACK POINTER INIT
0x00000004 - Vector table RESET ISR
0x00000040 - Vector table CLOCK ISR
0x000000DC - First usable address
0x0007FFFF - End of code ROM (512 KiB)
0x20000000 - Beginning of data RAM
0x2000FFFF - End of data RAM (64 KiB)
```