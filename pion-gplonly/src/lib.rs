// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! Attribute marker for items only to be used by GPL code.
//!
//! For items not marked in such way, a special provision is made in the
//! project's license to allow usage and linkage of these items in possibly
//! non-free code.
#![no_std]

extern crate proc_macro;
use proc_macro::TokenStream;

/// Declares an item to be GPL-only. These symbols may only be used from
/// GPL-compliant code.
///
/// *Note that this is not the legally binding agreement, but just a brief,
/// non-authoritative summary. For the legally binding agreement, see the
/// accompanying license text in COPYING in the project root.*
#[proc_macro_attribute]
pub fn gpl_only(_attr: TokenStream, item: TokenStream) -> TokenStream {
    // No operation -- just a symbolic act expressing intent :^)
    item
}
