// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! This is just a wrapper crate, check the documentation for
//! [pion-lib](pion_lib).
#![no_std]
#![no_main]

// This imports necessary symbols for the crate to compile, e.g. the
// panic handler.
#[doc(hidden)]
use pion_lib::*;

pub mod api {
    //! Public API for applications.
}
