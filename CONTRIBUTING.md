# Contributing

This file contains general guidelines for contribution.

## Safety

Every block of `unsafe` needs a comment at the beginning which argues the
safety of the block.

```rs
unsafe {
    // SAFETY: Because we uphold the contract here because of ... and ...
    do_dangerous_things();
}
```

Prefer to keep `unsafe` blocks small. Consider splitting `unsafe` blocks into
multiple blocks if there is no direct connection between the contents or when
a lot of safe code would be pulled in.

## Formatting

Use `cargo fmt`. Using a tool for formatting takes away responsibility and
potential arguments. Yay!

## Documenting

Everything that is publicly available must be documented fully. For anything
private it's the authors choice. Generally, implementation details do not need
to receive doc-comments, but usually profit more from regular comments.

`unsafe` items have to have at least the `Safety` section, even when private.
The exception are implementations of `unsafe` functions, when the trait's
invariant isn't made more concrete for the implementation.

Anything that is declared `pub` is expected to be used by modules and module
authors that might treat your module as a black box. As such they are not
expected to dive deep into the implementation details.

Instead, the documentation should give them all they need to know, including
invariants.

### Format

The following format shall be used for documentation comments. It is to be
understood as the minimum required amount of verbosity. Of course, you can
always add more paragraphs, explaining more, if you think it helps users of your
module.

```rs
/// A high-level description of what this is.    <- Document public structs
///
/// # Type Parameters                            <- Document type parameters
///
/// * `T` - A description of what this is used for.
///
/// # Lifetime                                   <- Document lifetimes
///
/// * `'a` - A description of what this affects.
///
/// # Examples                                   <- Optional: Provide examples
///
/// ```
/// let y = MyFoo::new();
/// let x = MyStruct::create(&y);
/// x.bar();
/// ```
pub struct MyStruct<'a, T: Foo> {
    /// A description of what is stored here.    <- Document public fields
    pub field: &'a T,
    priv_field: Secret
}

/// A high-level description of what this is.    <- Document public traits
pub trait MyTrait {}

/// A high-level description of what this is.    <- Document public enums
pub enum MyEnum {
    /// A description of what this is.           <- Document variants
    ///
    /// # Variant Fields
    ///
    /// * A description of what is stored here.  <- Document variant fields
    Abc(i32),
    /// A description of what this is.
    Def {
        /// A description of what is stored here.
        ///
        /// Note this documentation is applied to the field directly.
        x: usize,
        /// A description of what is stored here.
        y: usize,
    },
    /// A description of what this is.
    Ghi,
}

/// Yada yada.
///
/// # Tuple Fields
///
/// * `0` - A description of what is stored here.
pub struct MyTup(pub Bar, Baz);

/// A description of why this is provided here.  <- Document public reexports
pub use something::That;

/// A description of what this represents.       <- Document public type aliases
pub type Cookie = (i32, i32);

// Do not add a doc comment here, instead use //! inside the module.
pub mod foo;

pub mod bar {
    //! A description of what is contained.      <- Document public modules
}

impl<'a, T: Foo> MyTrait for MyStruct<'a, T> {}

impl<'a, T: Foo> MyStruct<'a, T> {
    /// A description of what this does.         <- Document public functions
    ///
    /// # Arguments                              <- Document function arguments
    ///
    /// * `bar` - A description of what is needed here.
    ///
    /// # Returns                                <- Document return values
    ///
    /// A description of what is returned.
    ///
    /// # Panics                                 <- Document panics
    ///
    /// A description of when the function panics.
    pub fn do_thing(&self, bar: u32) -> i32 { /*...*/ }

    /// A description of what this does.
    ///
    /// # Safety                                 <- Document safety requirements
    ///
    /// A thorough descriptions of the invariants that need to be upheld.
    pub unsafe fn do_things_dangerously(&self) { /*...*/ }

    /// Optional: Document private details
    fn secret_detail() { /*...*/ }
}
```

### Section order

The sections in a documentation comment shall be ordered for consistency:

1. (Not a section) Description
1. Type Parameters
1. Lifetimes
1. Tuple Fields
1. Variant Fields
1. Arguments
1. Returns
1. Panics
1. Safety
1. See Also
1. Examples