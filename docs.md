# Flashing

Connect wires: Battery hinge to bottom is SWDIO, SWCLK, (disconnected 3V3), GND

Flash software directly via cargo embed
```
$ cargo embed --release
```

# OpenOCD connection

Connect wires (see above)

Start OpenOCD "server"
```
$ openocd -f interface/stlink.cfg -f target/nrf52.cfg
```

## GDB Connection

Start `arm-none-eabi-gdb`

Connect to OpenOCD
```
target extended-remote localhost:3333
```

Reset and halt: `monitor reset halt`
Reset: `monitor reset`

Documentation on more: https://openocd.org/doc/html/General-Commands.html

Disassemble memory region: `disas 0xStart,0xEnd`

Show memory `x 0xWhere`

# Disassemble binary

```
$ arm-none-eabi-objdump -xds target/thumbv7em-none-eabihf/release/pion
```

Or with https://github.com/rust-embedded/cargo-binutils installed:
```
$ cargo objdump --release -- -xds
```

# Other projects

https://github.com/dbrgn/pinetime-rtic
https://github.com/ferrous-systems/zero-to-main
https://wiki.pine64.org/wiki/PineTime_Development