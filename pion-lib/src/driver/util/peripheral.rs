// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! Provides safe access to peripherals.
use super::cpu::wait::Wait;
use super::ticket::{TicketAccess, Ticketable};
use core::cell::Cell;
use core::mem::ManuallyDrop;
use core::ops::{Deref, DerefMut};

/// [Cell](core::cell::Cell)-like [Peripheral](Peripheral) container.
///
/// Multiple users can hold a reference to this type and safely obtain exclusive
/// access to the underlying [Peripheral](Peripheral) once desired.
///
/// # Type Parameters
///
/// * `T` - The [Peripheral](Peripheral) contained in the cell.
pub struct PeripheralCell<T: Peripheral> {
    cell: Cell<Option<T>>,
}

/// Implementation detail for convenient access to
/// [PeripheralCell](PeripheralCell)s.
///
/// Can be transparently used for access to the underlying
/// [Peripheral](Peripheral).
///
/// # Type Parameters
///
/// * `T` - The [Peripheral](Peripheral) accessible through this struct.
///
/// # Lifetimes
///
/// * `'cell` - The lifetime of the [PeripheralCell](PeripheralCell) from which
///             this guard was obtained.
pub struct PeripheralGuard<'cell, T: Peripheral> {
    cell: &'cell Cell<Option<T>>,
    val: ManuallyDrop<T>,
}

/// A trait for peripherals, e.g. hardware devices. Most drivers will implement
/// this trait.
pub trait Peripheral {
    /// The type of initialization data that is required for
    /// [create()](Peripheral::create()).
    type Init;

    /// Creates the peripheral, possibly without initializing it.
    ///
    /// # Arguments
    ///
    /// * `init` - The required initialization arguments.
    ///
    /// # Returns
    ///
    /// The created peripheral.
    ///
    /// # Safety
    ///
    /// A peripheral may require to be only created once. It is the callers
    /// responsibility to ensure that the requirements of the concrete
    /// peripheral are upheld.
    unsafe fn create(init: Self::Init) -> Self;
}

/// A trait for [Peripheral](Peripheral)s that implement immediate access from
/// special contexts.
///
/// This is mostly intended for interrupt service routines requiring (limited)
/// access to [Peripheral](Peripheral)s.
///
/// # Type Parameters
///
/// * `T` - The accessor for the [Peripheral](Peripheral). Note that this is not
///         necessarily the [Peripheral](Peripheral) itself as the driver might
///         not implement immediate access for all functionality of the
///         [Peripheral](Peripheral).
pub trait ImmediateAccess<T>: Peripheral {
    /// Immediately accesses a (possibly limited) instance of the
    /// [Peripheral](Peripheral), for use in contexts where regular ownership is
    /// unfeasible to apply (e.g. interrupt service routines).
    ///
    /// # Returns
    ///
    /// An accessor for immediate access. May be of another type than the
    /// peripheral itself.
    ///
    /// # Safety
    ///
    /// A [Peripheral](Peripheral) may provide immediate access, but this access
    /// is not necessarily safe. Callers are required to satisfy the
    /// implementation's requirements for safety. These requirements may even
    /// impose restrictions on the use of safe methods on the returned value.
    /// It is therefore advisable to not hold the immediate access for long (as
    /// is usual with interrupt service routines), in order to simplify
    /// reasoning about the safety of the usage.
    unsafe fn immediate_access() -> T;
}

/// Used for enums that differentiate peripheral instances.
///
/// Some peripherals may exist as multiple identical instances.
pub trait Instancer {
    /// Obtains the register offset for this instance from the base instance.
    ///
    /// # Returns
    ///
    /// The offset between two of the same registers of the base peripheral
    /// (the instance with the lowest register address) and this instance.
    fn get_offset(&self) -> u32;
}

impl<T: Peripheral> PeripheralCell<T> {
    /// Creates the [Peripheral](Peripheral) inside the cell without
    /// necessarily initializing it.
    ///
    /// # Arguments
    ///
    /// * `init` - The required initialization arguments.
    ///
    /// # Returns
    ///
    /// The created peripheral cell.
    ///
    /// # Safety
    ///
    /// The caller has to guarantee that the safety requirements for
    /// [Peripheral::create()] for `T` are satisfied.
    pub unsafe fn create(init: T::Init) -> Self {
        let v = unsafe {
            // SAFETY: Caller guarantees safety.
            T::create(init)
        };
        let cell = Cell::new(Some(v));
        Self { cell }
    }

    /// Obtains the contained [Peripheral](Peripheral), if it is available.
    ///
    /// # Returns
    ///
    /// A [PeripheralGuard](PeripheralGuard) object providing access to the
    /// underlying [Peripheral](Peripheral), if it is available.
    ///
    /// On destruction of the guard object, the [Peripheral](Peripheral) is
    /// returned into its cell automagically.
    pub fn get(&self) -> Option<PeripheralGuard<T>> {
        PeripheralGuard::create(&self.cell)
    }
}

impl<'cell, T> TicketAccess<'cell, T, PeripheralGuard<'cell, T>> for PeripheralCell<T>
where
    T: Peripheral + Ticketable,
{
    fn get_inner(&'cell self) -> PeripheralGuard<'cell, T> {
        loop {
            if let Some(guard) = self.get() {
                return guard;
            }
            Wait::once()
        }
    }

    fn try_get_inner(&'cell self) -> Option<PeripheralGuard<'cell, T>> {
        self.get()
    }
}

impl<'cell, T: Peripheral> PeripheralGuard<'cell, T> {
    fn create(cell: &'cell Cell<Option<T>>) -> Option<Self> {
        match cell.take() {
            Some(val) => Some(Self {
                cell,
                val: ManuallyDrop::new(val),
            }),
            None => None,
        }
    }
}

impl<T: Peripheral> Deref for PeripheralGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.val
    }
}

impl<T: Peripheral> DerefMut for PeripheralGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.val
    }
}

impl<T: Peripheral> Drop for PeripheralGuard<'_, T> {
    fn drop(&mut self) {
        let val = unsafe {
            // SAFETY: self is destroyed after drop, no further accesses.
            ManuallyDrop::take(&mut self.val)
        };
        self.cell.set(Some(val));
    }
}
