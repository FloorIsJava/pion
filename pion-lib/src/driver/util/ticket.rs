// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! Provides a ticketing mechanism for inter-peripheral dependencies.
use core::marker::PhantomData;
use core::ops::{Deref, DerefMut};

/// Represents errors that can occur when dealing with [Ticketable](Ticketable)s.
pub enum TicketError {
    /// The resource is currently not available for unspecified reasons.
    Unavailable,
    /// The required arguments are incompatible with the current state of the
    /// resource.
    Incompatible,
}

/// Provides access to a [Ticketable](Ticketable).
///
/// # Type Parameters
///
/// * `T` - The managed [Ticketable](Ticketable).
/// * `I` - The type through which access is provided.
///
/// # Lifetimes
///
/// * `'content` - The lifetime of the managed ticketable, its accessor and thus
///                of created [Ticket](Ticket)s.
pub trait TicketAccess<'content, T, I>
where
    T: Ticketable,
    I: DerefMut + Deref<Target = T>,
{
    /// Obtains access to the managed [Ticketable](Ticketable).
    ///
    /// Care must be taken, as this method may block indefinitely until access
    /// is safe.
    ///
    /// # Returns
    ///
    /// The access to the managed [Ticketable](Ticketable).
    fn get_inner(&'content self) -> I;

    /// Attempts to obtain access to the managed [Ticketable](Ticketable).
    ///
    /// This does never block.
    ///
    /// # Returns
    ///
    /// The access to the managed [Ticketable](Ticketable), if possible.
    fn try_get_inner(&'content self) -> Option<I>;
}

/// Helper trait that provides common operations for [TicketAccess](TicketAccess).
///
/// # Type Parameters
///
/// * `T` - The [Ticketable](Ticketable) of the associated
///         [TicketAccess](TicketAccess).
/// * `I` - The accessor type of the associated [TicketAccess](TicketAccess).
///
/// # Lifetimes
///
/// * `'content` - The lifetime of the managed ticketable, its accessor and thus
///                of created [Ticket](Ticket)s.
///
/// # Safety
///
/// This shall only be implemented by [TicketAccess](TicketAccess).
pub unsafe trait TicketAccessOperations<'content, T, I>
where
    T: Ticketable,
    I: DerefMut + Deref<Target = T>,
{
    /// The parent [TicketAccess](TicketAccess).
    type Parent: TicketAccess<'content, T, I>;

    /// Requires the managed [Ticketable](Ticketable) to be kept active.
    ///
    /// TODO: make this take &mut self?
    ///
    /// # Arguments
    ///
    /// * `args` - The required arguments.
    ///
    /// # Returns
    ///
    /// Either a [Ticket](Ticket) or a [TicketError](TicketError).
    ///
    /// The [Ticket](Ticket) represents the requirement. The requirement is
    /// released when the [Ticket](Ticket) is dropped.
    ///
    /// # See Also
    /// [Ticketable::require()](Ticketable::require())
    fn require(
        &'content self,
        args: T::Args,
    ) -> Result<Ticket<'content, T, Self::Parent, I>, TicketError>
    where
        Self: Sized;
}

/// Represents something that can be kept active by different users.
///
/// This differs from reference counted data in that it doesnt handle lifetime,
/// but functionality.
///
/// An example of where this is useful is shared devices, e.g. clocks which
/// should not be stopped when a subsystem that uses these devices stops, given
/// that other subsystems are still requiring the device.
pub trait Ticketable {
    /// The arguments to pass to [require()](Ticketable::require()).
    type Args;

    /// Requires this ticketable to be kept active.
    ///
    /// If there are no requests yet, activates this ticketable.
    ///
    /// # Arguments
    ///
    /// * `args` - The arguments required for activating, if needed.
    ///
    /// # Returns
    ///
    /// The success status of requiring the ticketable.
    ///
    /// # Safety
    ///
    /// If successful, must be released by a call to [release()](Ticketable::release())
    /// once the requirement is no longer relevant.
    unsafe fn require(&mut self, args: Self::Args) -> Result<(), TicketError>;

    /// Releases a single request to be kept active.
    ///
    /// If there are no requests anymore, may deactivate this ticketable.
    ///
    /// # Safety
    ///
    /// Must release a corresponding [require()](Ticketable::require()) call.
    unsafe fn release(&mut self);
}

/// Represents a requirement for some [Ticketable](Ticketable) to stay active.
///
/// # Type Parameters
///
/// * `T` - The [Ticketable](Ticketable) this ticket is associated with.
/// * `A` - The [TicketAccess](TicketAccess) via which this ticket was obtained.
/// * `I` - The accessor type of the associated [TicketAccess](TicketAccess).
///
/// # Lifetimes
///
/// * `'a` - The lifetime of the [TicketAccess](TicketAccess).
pub struct Ticket<'a, T, A, I>
where
    T: Ticketable,
    A: TicketAccess<'a, T, I>,
    I: DerefMut + Deref<Target = T>,
{
    access: &'a A,
    _marker: PhantomData<(T, I)>,
}

impl<'a, T, A, I> Drop for Ticket<'a, T, A, I>
where
    T: Ticketable,
    A: TicketAccess<'a, T, I>,
    I: DerefMut + Deref<Target = T>,
{
    fn drop(&mut self) {
        // *Must* release on drop, no later chance. So, if things go south,
        // block until we have access... This may very well deadlock the system
        // if the resource is used while this ticket is dropped.
        //
        // Only an issue when in interrupt handlers, though.
        let mut inner = self.access.get_inner();

        unsafe {
            // SAFETY: When an instance of Ticket is created, the associated
            //         ticketable is required.
            //         This call to release() releases this requirement, exactly
            //         once.
            inner.release();
        }
    }
}

unsafe impl<'a, T, I, X> TicketAccessOperations<'a, T, I> for X
where
    T: Ticketable,
    I: DerefMut + Deref<Target = T>,
    X: TicketAccess<'a, T, I>,
{
    type Parent = X;

    fn require(&'a self, args: T::Args) -> Result<Ticket<'a, T, X, I>, TicketError>
    where
        Self: Sized,
    {
        if let Some(mut x) = self.try_get_inner() {
            let res = unsafe {
                // SAFETY: The created ticket ensures an eventual release().
                x.require(args)
            };
            if let Err(e) = res {
                Err(e)
            } else {
                Ok(Ticket::<'a> {
                    access: self,
                    _marker: PhantomData,
                })
            }
        } else {
            Err(TicketError::Unavailable)
        }
    }
}
