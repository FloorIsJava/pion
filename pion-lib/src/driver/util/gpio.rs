// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! Provides GPIO functionality.
use core::ptr;

/// The states a [Pin](Pin) can have.
#[derive(Eq, PartialEq)]
pub enum State {
    /// A [Pin](Pin) that is low (GND, 'false').
    Low = 0,
    /// A [Pin](Pin) that is high (VCC, 'true').
    High = 1,
}

/// The available types of GPIO pins.
#[derive(Clone, Copy)]
pub enum PinType {
    /// The pin used by the push button to notify us of its state.
    PushButtonIn = 13,
    /// The enable signal for the push button.
    PushButtonOut = 15,
    /// The switch for the low luminosity screen backlight.
    LcdBacklightLow = 14,
    /// The switch for the mid luminosity screen backlight.
    LcdBacklightMid = 22,
    /// The switch for the high luminosity screen backlight.
    LcdBacklightHigh = 23,
    /// The chip select pin for the LCD screen.
    LcdChipSelect = 25,
    /// The reset pin for the LCD screen.
    LcdReset = 18,
}

/// The direction of a [Pin](Pin).
#[derive(Copy, Clone)]
pub enum Direction {
    /// The [Pin](Pin) is an input [Pin](Pin), taking data from components.
    Input = 0,
    /// The [Pin](Pin) is an output [Pin](Pin), providing data to components.
    Output = 1,
}

/// Determines the kind of pull resistor attached to a [Pin](Pin).
#[derive(Copy, Clone)]
pub enum Pull {
    /// No pull resistor is attached.
    None = 0,
    /// High-Z is pulled down to [Low](State::Low).
    Down = 1,
    /// High-Z is pulled up to [High](State::High).
    Up = 3,
}

/// Controls automatic [State](State) sensing.
#[derive(Copy, Clone)]
pub enum Sensing {
    /// Automatic [State](State) sensing is disabled.
    None = 0,
    /// Sensing checks for [High](State::High) [State](State).
    High = 2,
    /// Sensing checks for [Low](State::Low) [State](State).
    Low = 3,
}

/// Configuration for a GPIO [Pin](Pin).
pub struct PinConfig {
    mask: u32,
}

impl PinConfig {
    /// Creates a configuration from the provided values.
    ///
    /// # Arguments
    ///
    /// * `dir` - The [Direction](Direction) of the pin.
    /// * `pull` - The type of pull resistor that should be connected to the
    ///            pin.
    /// * `sense` - The type of [State](State) sensing that should be performed
    ///             on the pin.
    ///
    /// # Returns
    ///
    /// The created pin configuration.
    pub fn create(dir: Direction, pull: Pull, sense: Sensing) -> Self {
        let dir_value = dir as u32;
        let pull_value = pull as u32;
        let sense_value = sense as u32;

        let buf_value = match &dir {
            Direction::Input => 0, // Enable input buffer (0 = connected)
            Direction::Output => 1,
        };

        Self {
            mask: dir_value | (buf_value << 1) | (pull_value << 2) | (sense_value << 16),
        }
    }
}

/// Represents a GPIO pin.
pub trait Pin {
    /// Returns the [PinType](PinType) of this pin.
    ///
    /// # Returns
    ///
    /// The [PinType](PinType) of this pin.
    fn get_type(&self) -> PinType;
}

/// A pin that may be created only once.
///
/// # Safety
///
/// Implementations must not provide any means to get an instance of themselves,
/// other than via [create()](ExclusivePin::create()).
pub unsafe trait ExclusivePin: Pin {
    /// The [PinType](PinType) of this pin.
    const TYPE: PinType;

    /// Creates this exclusive pin.
    ///
    /// # Returns
    ///
    /// The created instance.
    ///
    /// # Safety
    ///
    /// May only be called once.
    unsafe fn create() -> Self;
}

/// Helper trait that provides common operations for [Pin](Pin)s.
///
/// # Safety
///
/// Shall only be blanket-implemented by [Pin](Pin).
pub unsafe trait PinOperations {
    /// Initializes the represented pin with the given [PinConfig](PinConfig).
    ///
    /// # Arguments
    ///
    /// * `config` - The [PinConfig](PinConfig).
    ///
    /// # Safety
    ///
    /// This is unsafe because:
    ///
    /// * The [PinConfig](PinConfig) is not checked for validity for the given
    ///   pin. Consult the hardware documentation for valid configurations.
    /// * Multiple instances may configure the same hardware pin at the same
    ///   time. It is a responsibility of the caller to prevent this, usually
    ///   by encapsulating all access to the pin in a driver.
    unsafe fn initialize(&mut self, config: PinConfig);

    /// Initializes the represented pin with the given [PinConfig](PinConfig),
    /// using interior mutability.
    ///
    /// # Arguments
    ///
    /// * `config` - The [PinConfig](PinConfig).
    ///
    /// # Safety
    ///
    /// This is unsafe because:
    ///
    /// * The [PinConfig](PinConfig) is not checked for validity for the given
    ///   pin. Consult the hardware documentation for valid configurations.
    /// * Multiple instances may configure the same hardware pin at the same
    ///   time. It is a responsibility of the caller to prevent this, usually
    ///   by encapsulating all access to the pin in a driver.
    /// * This operation _can be_ logically mutating. To not limit
    ///   close-to-hardware drivers that might need to do 'crazy' things with
    ///   the pin, the pin exposes interior mutability through the `*_force`
    ///   functions.
    unsafe fn initialize_force(&self, config: PinConfig);

    /// Writes the given [State](State) to the represented pin.
    ///
    /// # Arguments
    ///
    /// * `v` - The [State](State) to be written.
    ///
    /// # Safety
    ///
    /// This is unsafe because:
    ///
    /// * Requirements by the hardware protocol listening to the pin are not
    ///   checked.
    /// * Multiple instances may write to the same hardware pin at the same
    ///   time. It is a responsibility of the caller to prevent this (if this is
    ///   undesirable), usually by encapsulating all access to the pin in a
    ///   driver.
    unsafe fn write(&mut self, v: State);

    /// Writes the given [State](State) to the represented pin, using interior
    /// mutability.
    ///
    /// # Arguments
    ///
    /// * `v` - The [State](State) to be written.
    ///
    /// # Safety
    ///
    /// This is unsafe because:
    ///
    /// * Requirements by the hardware protocol listening to the pin are not
    ///   checked.
    /// * Multiple instances may write to the same hardware pin at the same
    ///   time. It is a responsibility of the caller to prevent this (if this is
    ///   undesirable), usually by encapsulating all access to the pin in a
    ///   driver.
    /// * This operation _can be_ logically mutating. To not limit
    ///   close-to-hardware drivers that might need to do 'crazy' things with
    ///   the pin, the pin exposes interior mutability through the `*_force`
    ///   functions.
    unsafe fn write_force(&self, v: State);

    /// Reads the [State](State) of the represented pin.
    ///
    /// # Returns
    ///
    /// The [State](State) of the represented pin.
    fn read(&self) -> State;
}

impl<T: ExclusivePin> Pin for T {
    fn get_type(&self) -> PinType {
        Self::TYPE
    }
}

const GPIO_BASE: u32 = 0x50000000;
const GPIO_OUTSET: *mut u32 = (GPIO_BASE + 0x508) as *mut u32;
const GPIO_OUTCLR: *mut u32 = (GPIO_BASE + 0x50C) as *mut u32;
const GPIO_IN: *mut u32 = (GPIO_BASE + 0x510) as *mut u32;
const GPIO_PINCFG: *mut u32 = (GPIO_BASE + 0x700) as *mut u32;

unsafe impl<T: Pin> PinOperations for T {
    // SAFETY: This is the one allowed implementation.

    unsafe fn initialize(&mut self, config: PinConfig) {
        unsafe {
            // SAFETY: Stronger requirements than initialize_force, no interior
            //         mutability is happening.
            self.initialize_force(config);
        }
    }

    unsafe fn initialize_force(&self, config: PinConfig) {
        let val = config.mask;
        let offset = self.get_type() as isize;
        unsafe {
            // SAFETY: Caller ensures config valid for this pin.
            ptr::write_volatile(GPIO_PINCFG.offset(offset), val);
        }
    }

    unsafe fn write(&mut self, v: State) {
        unsafe {
            // SAFETY: Stronger requirements than write_force, no interior
            //         mutability is happening.
            self.write_force(v);
        }
    }

    unsafe fn write_force(&self, v: State) {
        let shift = self.get_type() as isize;
        let val = 1u32 << shift;
        let addr = match v {
            State::High => GPIO_OUTSET,
            State::Low => GPIO_OUTCLR,
        };
        unsafe {
            // SAFETY: Caller respects protocol for this pin.
            ptr::write_volatile(addr, val);
        }
    }

    fn read(&self) -> State {
        let shift = self.get_type() as isize;
        let mask = 1u32 << shift;
        let reg_value = unsafe {
            // SAFETY: Reading the GPIO_IN register is safe as per hardware
            //         docs.
            ptr::read_volatile(GPIO_IN)
        };

        match (reg_value & mask) != 0 {
            true => State::High,
            false => State::Low,
        }
    }
}

/// Contains available [Pin](Pin)s.
pub mod pins {
    use super::*;

    /// The pin with [PinType](PinType) [PushButtonIn](PinType::PushButtonIn).
    pub struct PushButtonIn;
    /// The pin with [PinType](PinType) [PushButtonOut](PinType::PushButtonOut).
    pub struct PushButtonOut;
    /// The pin with [PinType](PinType) [LcdChipSelect](PinType::LcdChipSelect).
    pub struct LcdChipSelect;
    /// The pin with [PinType](PinType) [LcdReset](PinType::LcdReset).
    pub struct LcdReset;
    /// The pin with [PinType](PinType) [LcdBacklightLow](PinType::LcdBacklightLow).
    pub struct LcdBacklightLow;
    /// The pin with [PinType](PinType) [LcdBacklightMid](PinType::LcdBacklightMid).
    pub struct LcdBacklightMid;
    /// The pin with [PinType](PinType) [LcdBacklightHigh](PinType::LcdBacklightHigh).
    pub struct LcdBacklightHigh;

    macro_rules! exclusive {
        ($i:ident) => {
            unsafe impl ExclusivePin for $i {
                // SAFETY: Only instance given out is via create.

                const TYPE: PinType = PinType::$i;

                unsafe fn create() -> Self {
                    Self
                }
            }
        };
    }

    exclusive!(PushButtonIn);
    exclusive!(PushButtonOut);
    exclusive!(LcdChipSelect);
    exclusive!(LcdReset);
    exclusive!(LcdBacklightLow);
    exclusive!(LcdBacklightMid);
    exclusive!(LcdBacklightHigh);
}
