// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! Provides WFI (Wait for interrupt) functionality.

/// Provides access to waiting for conditions.
pub struct Wait {}

impl Wait {
    /// Waits for an unspecified amount of time, conserving power.
    ///
    /// Internally, this usually waits until either an interrupt or a spurious
    /// wakeup occurs.
    pub fn once() {
        unsafe {
            // SAFETY: Wait for interrupt.
            asm!("wfi", options(nomem, nostack));
        }
    }

    /// Waits until a condition becomes `true`.
    ///
    /// This is equivalent to running
    /// ```
    /// while !f() {
    ///     Wait::once();
    /// }
    /// ```
    ///
    /// Users have to make sure the condition can asynchronously become true,
    /// otherwise this may block forever.
    ///
    /// # Arguments
    ///
    /// * `f` - The predicate to check.
    pub fn until<F>(f: F)
    where
        F: Fn() -> bool,
    {
        while !f() {
            Self::once();
        }
    }

    /// Locks up the current thread of execution indefinitely.
    ///
    /// Intended to be used in cases of fatal errors.
    ///
    /// Equivalent to executing
    /// ```
    /// Wait::until(|| false);
    /// ```
    ///
    /// # Returns
    ///
    /// Never.
    pub fn lockup() -> ! {
        // If the optimizer is dumb, implementing the function
        // like this has potential to save operations.
        loop {
            Self::once();
        }
    }
}
