// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! Driver for the FPU extension.
use core::ptr;

pub mod f32;

/// Provides configuration access to the FPU.
pub struct Fpu {}

impl Fpu {
    const SYSTEM_CONTROL: u32 = 0xE000ED00;
    const CPACR: *mut u32 = (Self::SYSTEM_CONTROL + 0x88) as *mut u32;
    const FPDSCR: *mut u32 = (Self::SYSTEM_CONTROL + 0x23C) as *mut u32;

    const ENABLE_MASK: u32 = 0xF << 20;

    // AHP = 0
    // DN = 0
    // RMODE = 00
    const DEF_CLEARMASK: u32 = !(1 << 26 | 1 << 25 | 3 << 22);

    // FZ = 1
    const DEF_SETMASK: u32 = 1 << 24;

    /// Initializes the default FPU status control (FPDSCR).
    ///
    /// This sets the following status control settings for new FP contexts:
    /// - IEEE 754-2008 half-precision format
    /// - Default NaN is disabled
    /// - Flush-to-zero mode is enabled (non-IEEE-compliant)
    /// - Rounding mode is round towards nearest.
    pub fn initialize() {
        let old = unsafe {
            // SAFETY: Reading the FPDSCR is safe.
            ptr::read_volatile(Self::FPDSCR)
        };

        let new = (old | Self::DEF_SETMASK) & Self::DEF_CLEARMASK;
        unsafe {
            // SAFETY: This changes the default options to the documented
            //         values. Writing is always possible, and we are the only
            //         ones who write, so this is safe.
            ptr::write_volatile(Self::FPDSCR, new);
        }
    }

    /// Enables the FPU.
    ///
    /// # Safety
    ///
    /// Must not be used while configuring any other coprocessors.
    pub unsafe fn enable() {
        let old = unsafe {
            // SAFETY: Reading the CPACR is safe.
            ptr::read_volatile(Self::CPACR)
        };

        let new = old | Self::ENABLE_MASK;
        unsafe {
            // SAFETY: This enables the FPU via CP11 and CP10. Caller guarantees
            //         no modifications to CPACR during this function.
            ptr::write_volatile(Self::CPACR, new);
        }

        unsafe {
            // SAFETY: NOP-padding. Without this, vmrs does weird things.
            //         TODO: find out what exactly is needed here
            asm!("nop");
            asm!("nop");
        }

        let fpscr: u32;
        unsafe {
            // SAFETY: Coprocessor is enabled, so FPSCR can be read.
            asm!("vmrs {}, FPSCR", out(reg) fpscr, options(nostack, nomem));
        }

        let new = (fpscr | Self::DEF_SETMASK) & Self::DEF_CLEARMASK;
        unsafe {
            // SAFETY: FPSCR FP context settings are updated in this way.
            asm!("vmsr FPSCR, {}", in(reg) new, options(nostack, nomem));
        }
    }

    /// Checks whether the FPU is enabled.
    ///
    /// # Safety
    ///
    /// Must not be used while configuring any coprocessors.
    unsafe fn is_enabled() -> bool {
        let old = unsafe {
            // SAFETY: Reading the CPACR is safe.
            ptr::read_volatile(Self::CPACR)
        };

        (old & Self::ENABLE_MASK) == Self::ENABLE_MASK
    }
}
