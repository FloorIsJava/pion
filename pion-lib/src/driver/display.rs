// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! Software driver for ST7789V driver IC.

// --- Documentation ---
//
// Uses 4 line serial interface (?), all ports OUT.
// Transmission is MSB first. All-or-nothing wrt. CSX breaks.
// LCD_SCK            (P0.02) -> SPI Clock [SCL]
//                               only needed when communicating
// LCD_SDI            (P0.03) -> SPI MOSI (Sampled on rising edge) [SDA]
// LCD_BACKLIGHT_LOW  (P0.14) -> Low backlight (Active Low)
// LCD_RS             (P0.18) -> Command (0) vs. Data (1) [D/CX]
// LCD_BACKLIGHT_MID  (P0.22) -> Mid backlight (Active low)
// LCD_BACKLIGHT_HIGH (P0.23) -> High backlight (Active low)
// LCD_CS             (P0.25) -> Chip Select (Active low) [CSX]
// LCD_RESET          (P0.26) -> Display Reset (Active low) (>=10µs)
//
// -- Command write mode --
// One transmission byte, interpreted according to D/CX. D/CX=1 -> store byte in
// display data RAM.
// Start of transmission: falling edge of CSX. SCL doesnt matter at this point,
//   high or low.
// D/CX sampled on 8th rising edge of SCL.
// If after the 8 bits, CSX stays low, next byte starts at next rising edge of
//   SCL.
//
// -- Read function --
// Triggered after certain commands. Needs CSX to go high before the next write.
// Pins not connected (?).
//
// -- Data transfer --
// 16 bit color per pixel, RGB 5-6-5.
// Two methods for transferring frame data.
// 1. Write successive frames until a command occurs.
// 2. Write single frames.
//
// Color encoding RGB 5-6-5 chosen by command 0x3A 0x55 is as follows:
//   CSX falls, D/CX high, transmit R4..R0 G5..G0 B4..B0, two transmissions per
//   pixel.
//
// -- Commands --
// - Table 1 -
// NOP       0x00
// SWRESET   0x01 Affects registers, but not frame memory. Post-delay of 5ms
//                needed. In Sleep-IN mode, 120ms needed before Sleep-OUT.
//                Cannot be sent during transition to Sleep-OUT.
//                Leaves system in Sleep-IN mode.
// RDDID     0x04 -- IGNORED --
// RDDST     0x09 -- IGNORED --
// RDDPM     0x0a -- IGNORED --
// RDDMADCTL 0x0b -- IGNORED --
// RDDCOLMOD 0x0c -- IGNORED --
// RDDIM     0x0d -- IGNORED --
// RDDSM     0x0e -- IGNORED --
// RDDSDR    0x0f -- IGNORED --
// SLPIN     0x10 Enter minimum power consumption mode (Sleep-IN).
//                Memory unaffected. Post-delay of 5ms needed. 120ms before
//                Sleep-OUT. [DEFAULT]
// SLPOUT    0x11 Leave minimum power consumption mode (Sleep-OUT).
//                Post-delay of 5ms needed. 120ms before Sleep-IN.
// PTLON     0x12 Enables partial mode.
// NORON     0x13 Disables partial mode. [DEFAULT]
// INVOFF    0x20 Disables display inversion mode. [DEFAULT]
// INVON     0x21 Enables display inversion mode.
// GAMSET    0x26 Expects 0x0X to select gamma curve, based on bit in X.
//                [DEFAULT 0x01]
// DISPOFF   0x28 Blanks the display. No changes to memory. [DEFAULT]
// DISPON    0x29 Unblanks the display.
// CASET     0x2A Sets the column address boundaries. 0xXX 0xYY 0xZZ 0xWW where
//                0xXXYY is column start XS, 0xZZWW is column end XE.
//                0 < XS <= XE < 0xEF  if MV=0
//                0 < XS <= XE < 0x13F if MV=1
//                [DEFAULT XS=0x0]
//                [DEFAULT XE=0xEF, if MV=1 then 0x13F on SWRESET]
// RASET     0x2B Sets the row address boundaries. 0xXX 0xYY 0xZZ 0xWW where
//                0xXXYY is row start YS, 0xZZWW is row end YE.
//                0 < YS <= YE < 0x13F if MV=0
//                0 < YS <= YE < 0xEF if MV=1
//                [DEFAULT YS=0x0]
//                [DEFAULT YE=0x13F, if MV=1 then 0xEF on SWRESET]
// RAMWR     0x2C Starts writing to frame memory. Resets CR/PR to XS/YS.
//                [DEFAULT uninitialized]
// RAMRD     0x2E -- IGNORED --
// PTLAR     0x30 Defines partial display area. 0xXX 0xYY 0xZZ 0xWW where
//                0xXXYY is start row PSL, 0xZZWW is end row PEL.
//                [DEFAULT PSL=0x0, PEL=0x13F]
// VSCRDEF   0x33 Defines vertical scrolling area. 0xXX 0xYY 0xZZ 0xWW 0xVV 0xUU
//                where 0xXXYY is top fixed area TFA, 0xZZWW is vertical
//                scrolling area VSA, 0xVVUU is bottom fixed area BFA.
//                TFA + VSA + BFA = 320.
//                [DEFAULT TFA=0x0, VSA=0x140, BFA=0x0]
// TEOFF     0x34 Disable tearing effect output signal. [DEFAULT]
// TEON      0x35 Enable tearing effect output signal. 0xXX where boolean 0xXX
//                indicates whether H-blanking info is sent.
// MADCTL    0x36 Defines memory scanning directions. Parameter 0xXX, MSB first,
//                consists of
//                - MY  Page Address Order    0: TTB  1: BTT
//                - MX  Column Address Order  0: LTR  1: RTL
//                - MV  Page/Column Order:    0: NOR  1: REV
//                - ML  Line Address Order:   0: TTB  1: BTT
//                - RGB RGB vs BGR:           0: RGB  1: BGR
//                - MH  Disp. DLD Order:      0: LTR  1: RTL
//                - don't care
//                - don't care
//                [DEFAULT 0x0, not changed on SWRESET]
// VSCSAD    0x37 Set vertical scroll start address. 0xXX 0xYY where 0xXXYY
//                is VSP. This is the first line to be written after TFA.
//                [DEFAULT 0x0]
// IDMOFF    0x38 Disables idle mode. [DEFAULT]
// IDMON     0x39 Enables idle mode. 8 colors, determined by MSB. Lower frame
//                frequency.
// COLMOD    0x3A Sets RGB format. Parameter 0xXX, MSB first, consists of
//                - '0'
//                - '101' 65k RGB  '110' 262k RGB
//                - '0'
//                - '011' 12bit  '101' 16bit  '110' 18bit  '111' 16M trunc.
//                [DEFAULT 18bit. Color depth unspecified, no change on SWRESET]
// WRMEMC    0x3C Continues writing memory at last pointer.
// RDMEMC    0x3E -- IGNORED --
// STE       0x44 Sets tearing effect output signal on line 0xXXYY with 0xXX
//                0xYY. Only V-blanking info is sent. [DEFAULT 0x0]
// GSCAN     0x45 -- IGNORED --
// WRDISBV   0x51 Sets the display brightness. 0xXX, 0xFF is highest.
//                [DEFAULT 0x0]
// RDDISBV   0x52 -- IGNORED --
// WRCTRLD   0x53 Controls display brightness. Parameter 0xXX, MSB first,
//                consists of:
//                - '00'
//                - BCTRL Brightness control block       0: off  1: on
//                - '0'
//                - DD    Display dimming (only manual)  0: off  1: on
//                - BL    Backlight control              0: off  1: on
//                - '00'
//                [DEFAULT 0x0]
// RDCTRLD   0x54 -- IGNORED --
// WRCACE    0x55 Sets parameters for content based adaptive brightness control
//                and color enhancement. Parameter 0xXX, MSB first, consists of:
//                - CECTRL Color Enhancement Control  0: off  1: on
//                - '0'
//                - CE Color Enhancement  00: low  01: med  11: high
//                - '00'
//                - C CBAC Control  00: off  01: UI  10: Picture  11: Video
//                [DEFAULT 0x0]
// RDCABC    0x56 -- IGNORED --
// WRCABCMB  0x5E Sets minimum display brightness for CABC. 0xXX, 0xFF is
//                highest. [DEFAULT 0x0]
// RDCABCMB  0x5F -- IGNORED --
// RDABCSDR  0x68 -- IGNORED --
// RDID1     0xDA -- IGNORED --
// RDID2     0xDB -- IGNORED --
// RDID3     0xDC -- IGNORED --
//
// - Table 2 (needs EXTC or CMD2EN) -
// RAMCTRL   0xB0 -- RAM control. Parameters 0xXX 0xYY, MSB first, consist of:
//                   - '000'
//                   - RM RAM Access Selection  0: MCU  1: RGB
//                   - '00'
//                   - DM Display Mode  00: MCU  01: RGB  10: VSYNC  11: N/A
//                   - '11'
//                   - EPF Data Translation
//                     - 00: Shift R, B left 1 bit
//                     - 01: Shift R, B left 1 bit, set LSB to 1
//                     - 10: Shift R, B left 1 bit, set LSB to MSB
//                     - 11: Shift R, B left 1 bit, set LSBs to green LSB
//                   - ENDIAN Must be '0' for serial interface (MSB first)
//                   - RIM RGB bus width  0: 18bit  1: 6bit
//                   - MDT Must be '00' for serial interface
//                   [DEFAULT RM=0x0, DM=0x0, EPF=0x3, ENDIAN=0x0, RIM=0x0,
//                            MDT=0x0]
// RGBCTRL   0xB1 RGB Interface Control. 0xXX 0xYY 0xZZ where 0xXX, MSB first,
//                consists of:
//                - WO Direct Mode  0: RAM  1: Shift Register
//                - RCM RGB Interface Enable  00/01: MCU  10: RGB DE  11: RGB HV
//                - '0'
//                - VSPL VSYNC Signal Polarity  0: low active  1: high active
//                - HSPL HSYNC Signal Polarity  0: low active  1: high active
//                - DPL DOTCLK Signal Polarity  0: pos. edge  1: neg. edge
//                - EPL ENABLE Signal Polarity  0: high active  1: low active
//                Least significant 7 bits of 0xYY are VSYNC back porch VBP.
//                Minimum setting is 0x2.
//                Least significant 5 bits of 0xZZ are HSYNC back porch HBP.
//                Minimum setting is 0x4 in 18bit RGB interface, 0xC in 6bit
//                RGB interface.
//                [DEFAULT WO=0x0, RCM=0x2, VSPL=0x0, HSPL=0x0, DPL=0x0,
//                         EPL=0x0]
//                [DEFAULT VBP=0x2]
//                [DEFAULT HBP=0x14]
// PORCTRL   0xB2 Porch setting. 0xXX 0xYY 0xZZ 0xWW 0xVV where:
//                Least significant 7 bits of 0xXX are back porch in normal
//                mode BPA. Minimum setting is 0x1.
//                Least significant 7 bits of 0xYY are front porch in normal
//                mode FPA. Minimum setting is 0x1.
//                LSB of 0xZZ is separate porch control enable PSEN. 0 is
//                disable, 1 is enable.
//                Upper half-byte of 0xWW is back porch in idle mode BPB. The
//                minimum setting is 0x1.
//                Lower half-byte of 0xWW is front porch in idle mode FPB. The
//                minimum setting is 0x1.
//                Upper half-byte of 0xVV is back porch in partial mode BPC. The
//                minimum setting is 0x1.
//                Lower half-byte of 0xVV is front porch in partial mode FPC.
//                The minimum setting is 0x1.
//                [DEFAULT BPA=0xC, FPA=0xC, PSEN=0x0]
//                [DEFAULT BPB=0x3, FPB=0x3]
//                [DEFAULT BPC=0x3, FPC=0x3]
// FRCTRL1   0xB3 Frame rate control. 0xXX 0xYY 0xZZ where 0xXX, MSB first,
//                consists of:
//                - '000'
//                - FRSEN Separate frame rate control enable  0: off  1: on
//                - '00'
//                - DIV Frame rate divider  Divide by 00: 1  01: 2  10: 4  11: 8
//                Most significant 3 bits of 0xYY are inversion selection in
//                idle mode NLB.  000: dot inversion  111: column inversion
//                Least significant 5 bits of 0xYY are frame rate control in
//                idle mode RTNB. 60Hz is 0xF, 50Hz is 0x15, 40Hz is 0x1E.
//                Most significant 3 bits of 0xZZ are inversion selection in
//                partial mode NLC. Least significant 5 bits of 0xZZ are frame
//                rate control in partial mode RTNC. Analogous to NLB, RTNB.
//                [DEFAULT FRSEN=0x0, DIV=0x0]
//                [DEFAULT NLB=0x0, RTNB=0xF]
//                [DEFAULT NLC=0x0, RTNC=0xF]
// PARCTRL   0xB5 Partial mode control. Parameter 0xXX, MSB first, consists of:
//                - NDL Non-display area source output level  0: V63  1: V0
//                - '00'
//                - PTGISC Non-display area scan mode  0: normal  1: interval
//                - ISC Interval frequency  once every 2*ISC+1 frames
//                [DEFAULT NDL=0x0, PTGISC=0x0, ISC=0x0]
// GCTRL     0xB7 Gate control. See docs.
// GTADJ     0xB8 Gate ON timing adjustment. See docs.
// DGMEN     0xBA Digital gamma enable. Parameter 0xXX, MSB first, consists of:
//                - '00000'
//                - DGMEN  0: disable digital gamma  1: enable digital gamma
//                - '00'
//                [DEFAULT DGMEN=0x0]
// VCOMS     0xBB VCOMS setting. For voltage settings, see docs.
// LCMCTRL   0xC0 Settings for inverting some paramters. See docs.
// IDSET     0xC1 Set the chip ID. See docs.
// VDVVRHEN  0xC2 Enables setting VDV and VRH from commands. See docs.
// VRHS      0xC3 Set VRH. See docs.
// VDVS      0xC4 Set VDV. See docs.
// VCMOFSET  0xC5 Set VCOMS Offset. See docs.
// FRCTRL2   0xC6 Frame rate control. 0xXX where most significant 3 bits are
//                inversion selection in normal mode NLA.  000: dot inversion
//                111: column inversion.
//                Least significant 5 bits are frame rate control RTNA. 60Hz is
//                0xF, 50Hz is 0x15, 40Hz is 0x1E.
//                [DEFAULT NLA=0x0, RTNA=0xF]
// CABCCTRL  0xC7 Control pins of CABC. See docs.
// REGSEL1   0xC8 -- RESERVED --
// REGSEL2   0xCA -- RESERVED --
// PWMFRSEL  0xCC PWM frequency selection. See docs.
// PWCTRL1   0xD0 Power control. See docs.
// VAPVANEN  0xD2 Enable VAP/VAN signal output. See docs.
// CMD2EN    0xDF Command 2 Enable. 0x5A 0x69 0x02 0xXX where LSB of 0xXX is
//                command 2 enable CMD2EN.  0: table 2 disabled while EXTC is
//                low  1: table 2 is enabled when EXTC is low
//                [DEFAULT CMD2EN=0x0]
// PVGAMCTRL 0xE0 Positive voltage gamma control. See docs.
// NVGAMCTRL 0xE1 Negative voltage gamma control. See docs.
// DGMLUTR   0xE2 Digital gamma LUT for red. See docs.
// DGMLUTB   0xE3 Digital gamma LUT for blue. See docs.
// GATECTRL  0xE4 Gate control. See docs.
// SPI2EN    0xE7 SPI2 Enable. See docs.
// PWCTRL2   0xE8 Power Control 2. See docs.
// EQCTRL    0xE9 Equalize time control. See docs.
// PROMCTRL  0xEC Program mode control. See docs.
// PROMEN    0xFA Program mode enable. See docs.
// NVMSET    0xFC NVM Setting. See docs.
// PROMACT   0xFE Program action. See docs.
//
// -- Timing --
// Values are WRITE (READ). All times in nanoseconds. 8MHz: 125ns
// CSX setup: 15 (60)  CSX hold: 15 (65)  CSX high: 40
// SCL high: 15 (60)  SCL low: 15 (60) SCL period: 66 (150)
// DCX setup: 10  DCX hold: 10  (both before/after rising SCL)
// SDA setup: 10  SDA hold: 10  (both before/after rising SCL)
