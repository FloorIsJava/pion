// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! Driver for peripheral tasks.
use super::driver_util::peripheral::Instancer;
use core::ptr;

/// Represents a task for a peripheral.
pub trait Task {
    /// A pointer to the task register.
    const REGISTER: *mut u32;

    /// Triggers the associated task.
    ///
    /// # Safety
    ///
    /// Caller must ensure correct protocol is upheld for the peripheral.
    unsafe fn trigger() {
        unsafe {
            // SAFETY: Tasks are triggered by writing 1 to them.
            ptr::write_volatile(Self::REGISTER, 1);
        }
    }
}

/// Represents an instancable task for a peripheral.
///
/// # Type Parameters
///
/// * `T` - The instancing type to differentiate tasks.
pub trait InstancableTask<T: Instancer> {
    /// A pointer to the task register for the base instance.
    const REGISTER_BASE: u32;

    /// Triggers the associated task instance.
    ///
    /// # Arguments
    ///
    /// * `instance` - The instance of the task to be triggered.
    ///
    /// # Safety
    ///
    /// Caller must ensure correct protocol is upheld for the peripheral.
    /// Provided instance offset must be valid.
    unsafe fn trigger(instance: T) {
        let reg = (Self::REGISTER_BASE + instance.get_offset()) as *mut u32;
        unsafe {
            // SAFETY: Tasks are triggered by writing 1 to them.
            ptr::write_volatile(reg, 1);
        }
    }
}
