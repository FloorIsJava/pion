// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! A driver for high and low frequency SoC clocks.
use super::driver_util::peripheral::{Peripheral, PeripheralCell};
use super::task::Task;
use core::marker::PhantomData;
use core::ptr;

/// Clock sources for the [LowFrequencyClock](LowFrequencyClock).
pub enum LowFrequencyClockSource {
    /// Uses a resistor-capacitor oscillator (LFRC).
    RcOscillator,
    /// Synthesizes the signal from the high frequency clock (LFSYNT).
    Synthesized,
}

/// Clock sources for the high frequency clock.
pub enum HighFrequencyClockSource {
    /// An internal oscillator (HFINT).
    InternalOscillator,
}

/// Common clock interface.
///
/// # Type Parameters
///
/// * `T` - The type of clock source supported.
pub trait Clock<T> {
    /// Starts the clock from the given source.
    ///
    /// # Arguments
    ///
    /// * `source` - The clock source.
    fn start(&mut self, source: T);

    /// Stops the clock.
    fn stop(&mut self);

    /// Checks if the clock is running.
    ///
    /// # Returns
    ///
    /// `true` if and only if the clock is running.
    fn is_running(&self) -> bool;
}

/// Represents the low frequency clock (LFCLK).
pub struct LowFrequencyClock {
    marker: PhantomData<()>, // makes this struct private-to-create
}

/// Convenience type alias for holding a LowFrequencyClock.
pub type LowFrequencyClockCell = PeripheralCell<LowFrequencyClock>;

// Peripheral ID is 0 (0x40000000). Interrupt is EX0 = 0x10.
// We do not have external crystal oscillators (?).
const CLOCK_BASE: u32 = 0x40000000;

struct TaskLowFrequencyClockStart {}
impl Task for TaskLowFrequencyClockStart {
    const REGISTER: *mut u32 = (CLOCK_BASE + 0x8) as *mut u32;
}

struct TaskLowFrequencyClockStop {}
impl Task for TaskLowFrequencyClockStop {
    const REGISTER: *mut u32 = (CLOCK_BASE + 0xC) as *mut u32;
}

impl LowFrequencyClock {
    const LFCLKSRC: *mut u32 = (CLOCK_BASE + 0x518) as *mut u32;
    const LFCLKSTAT: *mut u32 = (CLOCK_BASE + 0x418) as *mut u32;
}

impl Peripheral for LowFrequencyClock {
    type Init = ();

    /// Creates the clock, in stopped state.
    ///
    /// # Safety
    ///
    /// Only one instance may be created. Storage of the instance is up to the
    /// caller.
    ///
    /// # See Also
    ///
    /// * [Peripheral::create()](Peripheral::create())
    unsafe fn create(_init: Self::Init) -> Self {
        LowFrequencyClock {
            marker: PhantomData,
        }
    }
}

impl Clock<LowFrequencyClockSource> for LowFrequencyClock {
    fn start(&mut self, source: LowFrequencyClockSource) {
        // SRC EXT BYP
        //   0   0   0  Normal operation, RC source
        //   2   0   0  Normal operation, synth source
        let src = match source {
            LowFrequencyClockSource::RcOscillator => 0,
            LowFrequencyClockSource::Synthesized => 2,
        };
        // TODO make this more idiomatic

        unsafe {
            // SAFETY: Part of the startup sequence of LFCLK.
            ptr::write_volatile(Self::LFCLKSRC, src);
        }

        unsafe {
            // SAFETY: Part of the startup sequence of LFCLK.
            TaskLowFrequencyClockStart::trigger();
        }
    }

    fn stop(&mut self) {
        if self.is_running() {
            unsafe {
                // SAFETY: Part of the stop sequence of LFCLK.
                TaskLowFrequencyClockStop::trigger();
            }
        }
    }

    fn is_running(&self) -> bool {
        let stat = unsafe {
            // SAFETY: Reading this register is allowed to query the state of
            //         LFCLK.
            ptr::read_volatile(Self::LFCLKSTAT)
        };

        const RUNNING_BIT: u32 = 0x10000;
        (stat & RUNNING_BIT) == RUNNING_BIT
    }
}

// impl Clock<HighFrequencyClockSource> for HighFrequencyClock {}
