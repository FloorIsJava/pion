// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! A driver for the SoC [RealTimeCounter](RealTimeCounter).
use super::driver_util::peripheral::{Instancer, Peripheral, PeripheralCell};
use super::task::InstancableTask;
use core::ptr;

/// The available [RealTimeCounter](RealTimeCounter) instances.
#[derive(Clone, Copy)]
#[repr(u32)]
pub enum RealTimeCounterInstance {
    /// The first instance, with 3 compare registers.
    Rtc0 = 0x0,
    /// The second instance, with 4 compare registers.
    Rtc1 = 0x6000,
    /// The third instance, with 4 compare registers.
    Rtc2 = 0x13000,
}

/// Represents a real time counter (RTCn).
pub struct RealTimeCounter {
    cfg: Config,
}

/// Convenience type alias for holding a [RealTimeCounter](RealTimeCounter).
pub type RealTimeCounterCell = PeripheralCell<RealTimeCounter>;

/// Errors related to the [RealTimeCounter](RealTimeCounter).
pub enum RealTimeCounterError {
    /// Indicates that the given frequency is out of the valid range.
    FrequencyRange,
    /// Indicates that the [RealTimeCounter](RealTimeCounter) is already started.
    AlreadyStarted,
}

// RTC0: Peripheral ID is 11 (0x4000B000). Interrupt is EX11 = 0x1B.
// RTC1: Peripheral ID is 17 (0x40011000). Interrupt is EX17 = 0x21.
// RTC2: Peripheral ID is 36 (0x40024000). Interrupt is EX36 = 0x34.

struct Config {
    status: u8,
}

impl Config {
    const START_BIT: u8 = 0x80;

    fn create(instance: RealTimeCounterInstance) -> Self {
        let rtc_num = match instance {
            RealTimeCounterInstance::Rtc0 => 0,
            RealTimeCounterInstance::Rtc1 => 1,
            RealTimeCounterInstance::Rtc2 => 2,
        };

        // Created as stopped
        Self { status: rtc_num }
    }

    fn instance(&self) -> RealTimeCounterInstance {
        match self.status & 0x3 {
            1 => RealTimeCounterInstance::Rtc1,
            2 => RealTimeCounterInstance::Rtc2,
            _ => RealTimeCounterInstance::Rtc0,
        }
    }

    fn is_started(&self) -> bool {
        (self.status & Self::START_BIT) == Self::START_BIT
    }

    fn set_started(&mut self, started: bool) {
        if started {
            self.status |= Self::START_BIT;
        } else {
            self.status &= !Self::START_BIT;
        }
    }
}

impl Instancer for RealTimeCounterInstance {
    fn get_offset(&self) -> u32 {
        *self as u32
    }
}

struct Start {}
impl InstancableTask<RealTimeCounterInstance> for Start {
    const REGISTER_BASE: u32 = RealTimeCounter::RTC_BASE;
}

impl RealTimeCounter {
    const RTC_BASE: u32 = 0x4000B000;

    /// Starts the RTC with the given frequency.
    ///
    /// Not every frequency can be exactly matched.
    /// Exact matches are only possible for frequencies `f` where the following
    /// equation produces an integral value of `p` in the range [0, 4095]:
    /// ```txt
    /// p = 32768 / f - 1
    /// ```
    ///
    /// Needs the LFCLK to be running.
    ///
    /// # Arguments
    ///
    /// * `frequency` - The desired counter frequency. Has to be in the range
    ///                 [8, 32768] Hz.
    ///
    /// # Returns
    ///
    /// The success status of the operation.
    pub fn start(&mut self, frequency: u16) -> Result<(), RealTimeCounterError> {
        if frequency < 8 || frequency > 32768 {
            return Err(RealTimeCounterError::FrequencyRange);
        }

        if self.cfg.is_started() {
            return Err(RealTimeCounterError::AlreadyStarted);
        }
        self.cfg.set_started(true);

        let prescaler = (32768.0 / frequency as f32) as u16 - 1;
        let reg = self.prescaler();
        unsafe {
            // SAFETY: Set the prescaler before triggering start, while the RTC
            //         is not started.
            ptr::write_volatile(reg, prescaler.into());
        }

        unsafe {
            // SAFETY: Start task is triggered after setting the prescaler.
            Start::trigger(self.cfg.instance());
        }

        Ok(())
    }

    fn prescaler(&self) -> *mut u32 {
        (Self::RTC_BASE + 0x508 + self.cfg.instance() as u32) as *mut u32
    }
}

impl Peripheral for RealTimeCounter {
    type Init = RealTimeCounterInstance;

    /// Creates the RTC, in stopped state.
    ///
    /// # Safety
    ///
    /// Only one instance may be created for each
    /// [RealTimeCounterInstance](RealTimeCounterInstance). Storage is up to the
    /// caller.
    ///
    /// # See Also
    ///
    /// * [Peripheral::create()](Peripheral::create())
    unsafe fn create(init: Self::Init) -> Self {
        RealTimeCounter {
            cfg: Config::create(init),
        }
    }
}
