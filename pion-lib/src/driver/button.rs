// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! A driver for the [Button](Button) on the side of the PineTime.
use super::util::gpio::{self, pins, PinOperations};
use super::util::peripheral::{Peripheral, PeripheralCell};

/// Represents the push button.
pub struct Button {
    pin_in: pins::PushButtonIn,
    pin_out: pins::PushButtonOut,
}

/// Convenience type alias for holding a button.
pub type ButtonCell = PeripheralCell<Button>;

impl Peripheral for Button {
    type Init = (pins::PushButtonIn, pins::PushButtonOut);

    /// Creates the button.
    ///
    /// # Safety
    ///
    /// Only one instance can be created as the [Pin](gpio::Pin)s in
    /// [Init](Self::Init) are [ExclusivePin](gpio::ExclusivePin)s.
    /// Thus this function is safe to use.
    ///
    /// # See Also
    ///
    /// * [Peripheral::create()](Peripheral::create())
    unsafe fn create(init: Self::Init) -> Self {
        let mut res = Button {
            pin_in: init.0,
            pin_out: init.1,
        };

        let cfg = gpio::PinConfig::create(
            gpio::Direction::Output,
            gpio::Pull::None,
            gpio::Sensing::None,
        );

        unsafe {
            // SAFETY: Valid configuration for PushButtonOut.
            // SAFETY: Pin only managed by this driver, and we're initializing.
            res.pin_out.initialize(cfg);
        }

        unsafe {
            // SAFETY: Write PushButtonOut to High to enable the button.
            // SAFETY: Pin only managed by this driver.
            res.pin_out.write(gpio::State::High);
        }

        let cfg = gpio::PinConfig::create(
            gpio::Direction::Input,
            gpio::Pull::Down,
            gpio::Sensing::None,
        );

        unsafe {
            // SAFETY: Valid configuration for PushButtonIn.
            // SAFETY: Pin only managed by this driver, and we're initializing.
            res.pin_in.initialize(cfg);
        }

        res
    }
}

impl Button {
    /// Checks whether the button is currently pressed down.
    ///
    /// # Returns
    ///
    /// `true` if and only if the button is currently pressed down.
    pub fn is_down(&self) -> bool {
        self.pin_in.read() == gpio::State::High
    }
}
