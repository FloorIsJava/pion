// Copyright 2021 FloorIsJava
//
// This file is part of Pion.
//
// Pion is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pion is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pion.  If not, see <https://www.gnu.org/licenses/>.

//! Operating system for the PineTime smart watch.
#![no_std]
#![feature(asm)]
#![feature(core_intrinsics)]
#![feature(lang_items)]
#![deny(missing_docs)]
#![deny(rustdoc::broken_intra_doc_links)]
#![deny(rustdoc::invalid_codeblock_attributes)]
#![deny(rustdoc::invalid_rust_codeblocks)]
#![deny(rustdoc::private_intra_doc_links)]
#![deny(unsafe_op_in_unsafe_fn)]

#[cfg(not(doc))]
mod driver;

/// Private module for crate `pion-lib`.
#[cfg(doc)]
pub mod driver;

use core::{mem, panic, ptr};
use driver::button;
use driver::cpu::fpu::Fpu;
use driver::cpu::wait::Wait;
use driver::soc::clock::{self, Clock};
use driver::soc::rtc;
use driver::util::gpio::{self, ExclusivePin};

/// Reset vector, called on SoC reset.
///
/// The reset vector is stored in the vector table via a linker script.
///
/// # Returns
///
/// Never. Only way to leave is by system reset, which lands in here, again.
#[no_mangle]
pub fn __reset_isr() -> ! {
    // These are technically valueless linker symbols, so the addresses
    // contain all values!
    extern "C" {
        // static mut __stext: u32;
        // static mut __etext: u32; // __srodata
        // static mut __erodata: u32;
        static mut __sdata: u32;
        static mut __edata: u32;
        static __sidata: u32;
        static mut __sbss: u32;
        static mut __ebss: u32;
        // static mut __sheap: u32;
        // static mut __estack: u32; // __eheap
        // static mut __sstack: u32;
    }

    // Clear BSS memory (zero-initialized static storage)
    unsafe {
        // SAFETY: The locations' validity is ensured by the linker script.
        let mut sbss: *mut u32 = &mut __sbss;
        let ebss: *mut u32 = &mut __ebss;
        while sbss < ebss {
            ptr::write_volatile(sbss, mem::zeroed());
            sbss = sbss.offset(1);
        }
    }

    // Initialize DATA memory from flash (loaded at __sidata in flash,
    // needs to go to RAM)
    unsafe {
        // SAFETY: The locations' validity is ensured by the linker script.
        let mut sdata: *mut u32 = &mut __sdata;
        let mut sidata: *const u32 = &__sidata;
        let edata: *mut u32 = &mut __edata;
        while sdata < edata {
            ptr::write_volatile(sdata, ptr::read_volatile(sidata));
            sdata = sdata.offset(1);
            sidata = sidata.offset(1);
        }
    }

    // Initialize the FPU
    Fpu::initialize();
    unsafe {
        // SAFETY: No other code that could manipulate coprocessors is running
        //         yet.
        Fpu::enable();
    }

    // Transfer control to main after the baseline invariants have been set up
    main();
}

/// Panic handler. Currently deadlocks the system.
#[panic_handler]
pub fn panic(_info: &panic::PanicInfo) -> ! {
    // We cannot really do much on panic (for now), so just freeze
    // the system
    Wait::lockup();
}

/// "Late" OS entry point, after core runtime invariants have been set up,
/// but before optional hardware has been initialized.
///
/// # Returns
///
/// Never.
#[inline(always)] // only used in __reset_isr
pub fn main() -> ! {
    let gpio_push_button_in = unsafe {
        // SAFETY: This is the only instance of this pin.
        gpio::pins::PushButtonIn::create()
    };
    let gpio_push_button_out = unsafe {
        // SAFETY: This is the only instance of this pin.
        gpio::pins::PushButtonOut::create()
    };

    let push_button = unsafe {
        // SAFETY: Particular implementation is safe.
        button::ButtonCell::create((gpio_push_button_in, gpio_push_button_out))
    };

    let lfclk = unsafe {
        // SAFETY: Only one instance may be created, this happens here.
        clock::LowFrequencyClockCell::create(())
    };

    let rtc = unsafe {
        // SAFETY: Only one instance may be created for Rtc0, this happens here.
        rtc::RealTimeCounterCell::create(rtc::RealTimeCounterInstance::Rtc0)
    };

    if let Some(mut lfclk) = lfclk.get() {
        lfclk.start(clock::LowFrequencyClockSource::RcOscillator);
    }

    if let Some(mut rtc) = rtc.get() {
        let _ = rtc.start(8);
    }

    // Just freeze... for now!
    loop {
        if let Some(push_button) = push_button.get() {
            push_button.is_down();
        }

        // Wait until events have to be processed (not yet implemented)
        Wait::once();
    }
}
